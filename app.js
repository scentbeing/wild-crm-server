require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const glob = require("glob");
const cors = require("cors");
const { graphqlHTTP } = require("express-graphql");
const expressPlayground =
  require("graphql-playground-middleware-express").default;
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { applyMiddleware } = require("graphql-middleware");
const { shield } = require("graphql-shield");
const shieldObj = require("./app/shield/shield")
const initialDbData = require("./app/models/initial")

module.exports = (db) => {
  const app = express();

  var corsOptions = {
    origin: "*",
    // origin: "http://localhost:8081",
  };

  // app.use(cors(corsOptions));
  // var whitelist = ['http://localhost:8000', 'http://localhost:8010']
  // var whitelist = ['*']
  // var corsOptions = {
  //   origin: function (origin, callback) {
  //     if (whitelist.indexOf(origin) !== -1) {
  //       callback(null, true)
  //     } else {
  //       callback(new Error('Not allowed by CORS'))
  //     }
  //   }
  // }

  app.use(cors(corsOptions));


  // parse requests of content-type - application/json
  app.use(bodyParser.json());

  // parse requests of content-type - application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: true }));

  //iterate through resolvers file in the folder
  let resolvers = [...glob.sync("app/schema/*/*/*.resolver.js"), ...glob.sync("app/schema/*/*/*/*.resolver.js"), ...glob.sync("app/schema/*/*/*/*/*.resolver.js")];
  let registerResolvers = [];
  for (const resolver of resolvers) {
    // add resolvers to array
    registerResolvers = [...registerResolvers, require("./" + resolver)];
  }
  //iterate through resolvers file in the folder
  let types = [...glob.sync("app/schema/*/*/*.type.js"), ...glob.sync("app/schema/*/*/*/*.type.js"), ...glob.sync("app/schema/*/*/*/*/*.type.js")];
  let registerTypes = [];
  for (const type of types) {
    // add types to array
    registerTypes = [...registerTypes, require("./" + type)];
  }
  //make schema from typeDefs and Resolvers with "graphql-tool package (makeExecutableSchema)"
  const schema = makeExecutableSchema({
    typeDefs: registerTypes, //merge array types
    resolvers: registerResolvers, //merge resolver type
  });


  // db.sequelize.sync();
  // force: true will drop the table if it already exists
  db.sequelize.sync({ force: false, logging: console.log }).then(() => {
    console.log("Drop and Resync Database with { force: true }");
    initialDbData();
  });

  const permissions = shield(shieldObj);

  const schemaWithPermissions = applyMiddleware(schema, permissions);

  app.use(
    "/graphql",
    graphqlHTTP({
      schema: schemaWithPermissions,
      graphiql: true,
    })
  );

  app.get("/playground", expressPlayground({ endpoint: "/graphql" }));

  // simple route
  app.get("/", (req, res) => {
    res.redirect("./playground");
  });

  return app

}

