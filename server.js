require("dotenv").config();
const makeApp = require("./app.js")
const db = require("./app/models");

const app = makeApp(db)

// set port, listen for requests
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
