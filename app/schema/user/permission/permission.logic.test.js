const makePermissionLogic = require("./permission.logic");
const mockedDB = require("../../../models/mocked")

const permissionLogic = makePermissionLogic(mockedDB);

test("Check permission.logic getOneById function.", async () => {
  const permission = await permissionLogic.getOneById({ id: 1 });
  expect(permission).toMatchObject({ id: 1, name: "user:read" })
})

test("Check permission.logic getManyWithPagination function.", async () => {
  const permissions = await permissionLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20 });
  expect(permissions.rows[0].name).toMatch("user:edit")
})

test("Check permission.logic getManyWithPagination association with user.", async () => {
  const permissions = await permissionLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20, userId:1 });
  expect(permissions.rows[0].name).toMatch("user:read")
})

test("Check permission.logic getManyWithPagination association with role.", async () => {
  const permissions = await permissionLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20, roleId: 1});
  expect(permissions.rows[1].name).toMatch("user:edit")
})

test("Check permission.logic addOne", async () => {
  const permission = await permissionLogic.addOne({ name: "test" });
  
  expect(permission.name).toMatch("test")
  expect(permission).toHaveProperty('id')
})

test("Check permission.logic addMany", async () => {
  const permissions = await permissionLogic.addMany({permissionNamesArray: [{ name: "permission1" }, {name: "permission2"}]});
  
  expect(permissions[0].id).toBe(1)
})

test("Check permission.logic updateOne", async () => {
  const permission = await permissionLogic.updateOne({id: 1, name: "asdf"});
  
  expect(permission.name).toMatch("asdf")
})

test("Check permission.logic deleteOne", async () => {
  const permission = await permissionLogic.deleteOne({id: 1, name: "asdf"});
  
  expect(permission.isDeleted).toBe(true)
})
