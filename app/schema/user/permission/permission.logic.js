/**
 * The functions responsible for handling the permission type.
 * @module permission_logic
 */

module.exports = (db) => {
  const Op = db.Sequelize.Op;

  /**
   * Find a permission by Id.
   * @param {Object} idObject - {id: 1} 
   * @returns {permission}
   * @example 
   *  await permissionLogic.getOneById({
   *    id
   *  })
   */
  const getOneById = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const permission = await db.permission.findOne({ where: { id, isDeleted: false } });

      resolve(permission.dataValues)
    })
  }

  /**
   * Find many permissions with pagination.
   * @param {searchParam} paginationObject - {q, page, pageSize}
   * @returns {pagination<permission>}
   * @example 
   *  await permissionLogic.getManyWithPagination({
   *    q: "search",
   *    page: 2,
   *    pageSize:2
   *  })
   */
  const getManyWithPagination = ({ q, page, pageSize, roleId, userId }) => {
    return new Promise(async (resolve, reject) => {
      page = page ? page - 1 : 0;
      pageSize = pageSize || 10;

      if (page < 0) {
        resolve({ error: true, message: new Error("Please start the page at 1.") });
      }
      if (pageSize < 0 || pageSize >= 100) {
        resolve({ error: true, message: new Error("Please keep pageSize inbetween 1 - 100.") });
      }

      const offset = page * pageSize;
      const limit = pageSize;

      let search = {
        where: {
          isDeleted: false,
        },
      };

      if (roleId) {
        search = Object.assign(search, {
          include: {
            model: db.roleManyPermission,
            where: { roleId, isDeleted: false },
          }
        })
      }

      if (userId) {
        search = Object.assign(search, {
          include: {
            model: db.userManyPermission,
            where: { userId, isDeleted: false },
          }
        })
      }

      if (q) {
        search = Object.assign(search, {
          where: {
            name: {
              [Op.like]: "%" + q + "%",
            },
          },
        });
      }

      search.offset = offset;
      search.limit = limit;

      const permissions = await db.permission.findAndCountAll(search);
      permissions.page = page + 1;
      permissions.pageSize = pageSize;
      permissions.pageCount = Math.ceil(
        permissions.count / permissions.pageSize
      );
      permissions.rows = permissions.rows.map(p => p.dataValues)

      resolve(permissions)
    })
  }

  /**
   * Save a permission.
   * @param {permission} permissionObject - { name } It takes all the "permission" properties except id.
   * @returns {permission} - It returns the same object but with an id.
   * @example 
   *  await permissionLogic.addOne({
   *    name: "name",
   *  })
   */
  const addOne = ({ name }) => {
    return new Promise(async (resolve, reject) => {

      const newPermission = db.permission.build({ name: name });

      resolve(await newPermission.save())
    })
  }

  /**
   * Save many permissions.
   * @param {Array<permissions>} ArrayOfPermissions - [{ name }] The array ojbect takes all the "permission" properties except id.
   * @returns {boolean} - The result is true or false for the completion of the saves.
   * @example 
   *  await permissionLogic.addMany([{
   *    name: "name",
   *  }])
   */
  const addMany = ({ permissionNamesArray }) => {
    return new Promise(async (resolve, reject) => {

      const newPermissions = await db.permission.bulkCreate(permissionNamesArray);

      resolve(newPermissions)
    })
  }

  /**
   * Update a permission.
   * @param {permission} permissionObject - { id, name } It takes all the "role" properties. Id is required.
   * @returns {permission} 
   * @example 
   *  await permissionLogic.updateOne([{
   *    id,
   *    name: "name",
   *  }])
   */
  const updateOne = ({ id, name }) => {
    return new Promise(async (resolve, reject) => {

      const permission = await db.permission.update(
        { name: name },
        {
          where: { id: id, isDeleted: false },
          returning: true
        }
      );

      resolve(permission[0] !== 0 ? permission[1][0].dataValues : null)
    })
  }

  /**
   * Delete a permission. A soft delete from the column "is_deleted" becoming true.
   * @param {Object} idObject - { id } Id is required.
   * @returns {permission} 
   * @example 
   *  await permissionLogic.deleteOne({
   *    id,
   *  })
   */
  const deleteOne = ({ id }) => {
    return new Promise(async (resolve, reject) => {

      const permission = await db.permission.update(
        { isDeleted: true },
        {
          where: { id: id, isDeleted: false },
          returning: true,
        }
      );

      resolve(permission[0] !== 0 ? permission[1][0].dataValues : null)
    })
  }


  return {
    getOneById,
    getManyWithPagination,
    addOne,
    addMany,
    updateOne,
    deleteOne,
  }

}


