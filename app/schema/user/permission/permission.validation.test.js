const makeUserValidation = require("./permission.validation");
const mockedDB = require("../../../models/mocked")

const permissionValidation = makeUserValidation(mockedDB);

test("Check permission.validation isIdValid function.", async () => {
  const isIdValid = await permissionValidation.isIdValid(1);

  expect(isIdValid.result).toBe(true)
})

test("Check permission.validation areIdsValid function.", async () => {
  const areIdsValid = await permissionValidation.areIdsValid([1, 2, 3000000000000]);

  expect(areIdsValid.result).toBe(false)
})

test("Check permission.validation isNameTaken function.", async () => {
  const isNameTaken = await permissionValidation.isNameTaken("user:manage");

  expect(isNameTaken.result).toBe(false)
})
