/**
 * The functions responsible for validating the permission type.
 * @module permission_validation
 */

module.exports = (db) => {

  /**
   * Check validation of the permissionId. It will return true if the permissionId is real.
   * @param {number} id
   * @returns {boolean}
   * @example 
   *  await permissionValidation.isIdValid(id)
   */
  const isIdValid = (id) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingPermission = await db.permission.findOne({
        where: {
          id,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingPermission !== null })
    });
  }

  /**
   * Check validation of many permissionIds. It will return true if all ids are true.
   * @param {Array<number>} idArray
   * @returns {boolean}
   * @example 
   *  await permissionValidation.areIdsValid([1, 2, 3])
   */
  const areIdsValid = (idArray) => {
    return new Promise(async (resolve, reject) => {

      const selectedPermissions = await db.permission.findAndCountAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: idArray
          },
          isDeleted: false
        }
      })

      resolve({ result: idArray.length === selectedPermissions.count })
    })
  }

  /**
   * If the name of a permission is already taken, then it returns true. If you want a name available for adding a permission, then you want false return.
   * @param {string} name
   * @returns {boolean}
   * @example 
   *  await permissionValidation.isNameTaken("name")
   */
  const isNameTaken = ({ name }) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingPermission = await db.permission.findOne({
        where: {
          name: name,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingPermission !== null })
    })
  }

  return {
    isIdValid,
    areIdsValid,
    isNameTaken,
  }
}
