const gql = require("graphql-tag");
const { paginationType } = require("../../utils");

const applicationType = gql`

  ${paginationType("PermissionPaginationType", "PermissionType")}

  type PermissionType {
    id: ID
    name: String
  }

  input PermissionInput {
    id: ID
    name: String
  }
  
  type Query {
    permission(id: ID): PermissionType
    permissionMany(q: String, page: Int, pageSize: Int): PermissionPaginationType
  }
  type Mutation {
    permissionAdd(name: String!): PermissionType
    permissionUpdate(name: String!, id: Int!): PermissionType
    permissionDelete(id: Int!): PermissionType
  }
`;
module.exports = applicationType;
