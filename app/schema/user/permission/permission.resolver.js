const db = require("../../../models")
const makePermissionLogic = require("./permission.logic")
const makePermissionValidation = require("./permission.validation")

const permissionLogic = makePermissionLogic(db);
const permissionValidation = makePermissionValidation(db)

const permissionResolver = {
  Query: {
    permission: async (parent, args) => {
      const permission = await permissionLogic.getOneById({
        id: args.id
      })

      return permission
      // return await db.permission.findOne({ where: { id: args.id } });
    },
    permissionMany: async (parent, args) => {
      const permissions = await permissionLogic.getManyWithPagination({
        q: args.q,
        page: args.page,
        pageSize: args.pageSize
      })

      if (permissions.error) {
        return Error(permissions.message)
      }

      return permissions
    }
  },
  Mutation: {
    permissionAdd: async (parent, args) => {
      const isPermissionNameTaken = await permissionValidation.isNameTaken({
        name: args.name
      })

      if (isPermissionNameTaken.result) {
        return Error("A permission with that name already exist.");
      }

      const permission = await permissionLogic.addOne({
        name: args.name
      })

      return permission
    },
    permissionUpdate: async (parent, args) => {
      const isPermissionNameTaken = await permissionValidation.isNameTaken({
        name: args.name
      })

      if (isPermissionNameTaken.result) {
        return Error("A permission with that name already exist.");
      }

      const permission = await permissionLogic.updateOne({
        id: args.id,
        name: args.name
      })

      return permission ? permission : Error("Nothing to update.")
    },
    permissionDelete: async (parent, args) => {
      const permission = await permissionLogic.deleteOne({
        id: args.id
      })

      return permission ? permission : Error("Nothing to delete.")
    },
  },
};
module.exports = permissionResolver;
