const makeRoleLogic = require("./role.logic");
const mockedDB = require("../../../models/mocked")

const roleLogic = makeRoleLogic(mockedDB);

test("Check role.logic getOneById function.", async () => {
	const role = await roleLogic.getOneById({ id: 1 });
  expect(role.id).toBe(1)
  expect(role.name).toMatch("user:manage")
})

test("Check role.logic getManyWithPagination function.", async () => {
	const roles = await roleLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20 });
	expect(roles.rows[0].name).toMatch("task:manage")
})

test("Check role.logic getManyWithPagination association with user.", async () => {
	const roles = await roleLogic.getManyWithPagination({ page: 0, pageSize: 20, userId: 1 });
	expect(roles.rows[0].name).toMatch("user:manage")
})

test("Check role.logic addOne", async () => {
	const role = await roleLogic.addOne({ name: "test" });

	expect(role.name).toMatch("test")
	expect(role).toHaveProperty('id')
})

test("Check role.logic addMany", async () => {
	const roles = await roleLogic.addMany({ roleNamesArray: [{ name: "role1" }, { name: "role2" }] });

	expect(roles[0].id).toBe(1)
})

test("Check role.logic updateOne", async () => {
	const role = await roleLogic.updateOne({ id: 1, name: "asdf" });

	expect(role.name).toMatch("asdf")
})

test("Check role.logic deleteOne", async () => {
	const role = await roleLogic.deleteOne({ id: 1});

	expect(role.isDeleted).toBe(true)
})
