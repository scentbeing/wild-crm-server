/**
 * The functions responsible for handling the user type.
 * @module role_logic
 */

module.exports = (db) => {
  const Op = db.Sequelize.Op;
  /**
   * Find a role by Id.
   * @param {Object} idObject - {id: 1} 
   * @returns {role}
   * @example 
   *  await roleLogic.getOneById({
   *    id
   *  })
   */
  const getOneById = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const record = await db.role.findOne({ where: { id: id, isDeleted: false } });

      resolve(record);
    });
  }

  /**
   * Find many roles with pagination.
   * @param {searchParam} paginationObject - {q, page, pageSize}
   * @returns {pagination<role>}
   * @example 
   *  await roleLogic.getManyWithPagination({
   *    q: "search",
   *    page: 2,
   *    pageSize:2
   *  })
   */
  const getManyWithPagination = ({ q, page, pageSize, userId }) => {
    return new Promise(async (resolve, reject) => {

      page = page ? page - 1 : 0;
      pageSize = pageSize || 10;

      if (page < 0) {
        return resolve({ error: true, humanReadable: true, text: "Please start the page at 1." });
      }

      if (pageSize < 0 || pageSize >= 100) {
        return resolve({ error: true, humanReadable: true, text: "Please keep pageSize inbetween 1 - 100." });
      }

      const offset = page * pageSize;
      const limit = pageSize;

      let search = {
        where: {
          isDeleted: false,
        },
      };

      if (userId) {
        search = Object.assign(search, {
          include: {
            model: db.userManyRole,
            where: { userId, isDeleted: false },
          }
        })
      }

      if (q) {
        search = {
          where: {
            name: {
              [Op.like]: "%" + q + "%",
            },
            isDeleted: false,
          },
        };
      }

      search.offset = offset;
      search.limit = limit;

      const roles = await db.role.findAndCountAll(search);
      roles.page = page + 1;
      roles.pageSize = pageSize;
      roles.pageCount = Math.ceil(roles.count / roles.pageSize);

      resolve(roles);
    });
  }


  /**
   * Save a role. This includes the permissions.
   * @param {role} roleObject - { name, roleMany } It takes all the "user" properties except id.
   * @returns {role} - It returns the same object but with an id.
   * @example 
   *  await roleLogic.addOne({
   *    name: "name",
   *    permissionMany: [{id:2}]
   *  })
   */
  const addOne = ({ name, roleMany }) => {
    return new Promise(async (resolve, reject) => {

      const newRole = db.role.build({ name: name });

      resolve(await newRole.save())
    })
  }

  /**
   * Save many roles. This includes the permissions.
   * @param {Array<role>} ArrayOfRoles - [{ name, roleMany }] It takes all the "role" properties except id.
   * @returns {boolean} - The result is true or false for the completion of the saves.
   * @example 
   *  await roleLogic.addMany([{
   *    name: "name",
   *    permissionMany: [{id:2}]
   *  }])
   */
  const addMany = ({ roleNamesArray }) => {
    return new Promise(async (resolve, reject) => {

      const newRoles = await db.role.bulkCreate(roleNamesArray.map(roleName => ({
        name: roleName,
      })));

      resolve(newRoles)
    })
  }

  /**
   * Update a role. There is no functionality for updating permissions here.
   * @param {role} roleObject - { id, name } It takes all the "role" properties. Id is required.
   * @returns {role} 
   * @example 
   *  await roleLogic.updateOne([{
   *    id,
   *    name: "name",
   *  }])
   */
  const updateOne = ({ id, name }) => {
    return new Promise(async (resolve, reject) => {

      const role = await db.role.update(
        { name: name },
        {
          where: { id: id, isDeleted: false },
          returning: true
        }
      );

      resolve(role[0] !== 0 ? role[1][0].dataValues : null)
    })
  }

  /**
   * Delete a role. A soft delete from the column "is_deleted" becoming true.
   * @param {Object} idObject - { id } Id is required.
   * @returns {user} 
   * @example 
   *  await roleLogic.deleteOne({
   *    id,
   *  })
   */
  const deleteOne = ({ id }) => {
    return new Promise(async (resolve, reject) => {

      const role = await db.role.update(
        { isDeleted: true },
        {
          where: { id, isDeleted: false },
          returning: true,
          // plain: true,
        }
      );

      resolve(role[0] !== 0 ? role[1][0].dataValues : null)
    })
  }

  /**
   * Add a permission to a role. 
   * @param {Object} rolePermissionIdObject - { roleId, permissionId } Both are required.
   * @returns {user} 
   * @example 
   *  await roleLogic.addPermission({
   *    roleId,
   *    permissionId,
   *  })
   */
  const addPermissionOne = ({ roleId, permissionId }) => {
    return new Promise(async (resolve, reject) => {
      const newRole = db.roleManyPermission.build({ roleId, permissionId });

      resolve(await newRole.save())
    });
  }

  /**
   * Add many permissions to a role. 
   * @param {number} roleId - Required
   * @param {Array<Object>} permissionIdArray - [{ permissionId }] Required.
   * @returns {user} 
   * @example 
   *  await roleLogic.addPermission(roleId, [{
   *    permissionId,
   *  },{
   *    permissionId,
   *  },
   * ])
   */
  const addPermissionMany = (roleId, permissionManyArray) => {
    return new Promise(async (resolve, reject) => {

      const newPermissionManyForRole = await db.roleManyPermission.bulkCreate(permissionManyArray.map(permission => ({
        roleId: roleId,
        permissionId: permission.id,
      })));

      resolve(true)
    });
  }

  /**
   * Delete a permission from a role. 
   * @param {Object} RolePermissionIdObject - { roleId, permissionId } Both are required.
   * @returns {role} 
   * @example 
   *  await roleLogic.deletePermission({
   *    roleId,
   *    permissionId,
   *  })
   */
  const deletePermissionOne = ({ roleId, permissionId }) => {
    return new Promise(async (resolve, reject) => {
      const role = await db.roleManyPermission.update(
        { isDeleted: true },
        {
          where: { roleId, permissionId, isDeleted: false },
          returning: true,
          plain: true,
        }
      );

      resolve(role[1])
    });
  }

  /**
   * Delete many permissions from a role. 
   * @param {number} roleId - Required
   * @param {Array<Object>} permissionIdArray - [{ permissionId }] Required.
   * @returns {user} 
   * @example 
   *  await roleLogic.deletePermissionMany(roleId, [
   *    permissionId,
   *  }])
   */
  const deletePermissionMany = (roleId, permissionIdsArray) => {
    return new Promise(async (resolve, reject) => {

      for (var i = 0; i < permissionIdsArray.length; i++) {
        const role = await db.roleManyPermission.update(
          { isDeleted: true },
          {
            where: { roleId, permissionId: permissionIdsArray[i], isDeleted: false },
            returning: true,
            plain: true,
          }
        );
      }

      resolve(true)
    });
  }

  return {
    // self
    getOneById,
    getManyWithPagination,
    addOne,
    addMany,
    updateOne,
    deleteOne,
    // permissions
    addPermissionOne,
    addPermissionMany,
    deletePermissionOne,
    deletePermissionMany,
  }

}

