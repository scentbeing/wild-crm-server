const gql = require("graphql-tag");
const { paginationType } = require("../../utils");

const roleType = gql`

  ${paginationType("RolePaginationType", "RoleType")}

  type RoleType {
    id: ID
    name: String
    permissionMany(q: String, page: Int, pageSize: Int): PermissionPaginationType
  }

  input RoleInput {
    id: ID
    name: String
    permissionMany: [PermissionInput]
  }
  
  type Query {
    role(id: ID): RoleType
    roleMany(q: String, page: Int, pageSize: Int): RolePaginationType
  }
  type Mutation {
    roleAdd(name: String!, permissions: [PermissionInput]): RoleType
    roleUpdate(id: Int!, name: String!): RoleType
    roleDelete(id: Int!): RoleType

    roleAddPermission(roleId: Int!, permissionId: Int!) : PermissionType
    roleRemovePermission(roleId: Int!, permissionId: Int!) : PermissionType
  }
`;
module.exports = roleType;