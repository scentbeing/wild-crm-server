
/**
 * The functions responsible for validating the role type.
 * @module role_validation
 */


module.exports = (db) => {
  const Op = db.Sequelize.Op;
  /**
   * Check validation of the roleId. It will return true if the roleId is real.
   * @param {number} id
   * @returns {boolean}
   * @example 
   *  await roleValidation.isIdValid(id)
   */
  const isIdValid = (id) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingRole = await db.role.findOne({
        where: {
          id: id,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingRole !== null })
    });
  }

  /**
   * Check validation of many roleIds. It will return true if all roleIds are true.
   * @param {Array<number>} idArray
   * @returns {boolean}
   * @example 
   *  await roleValidation.areIdsValid([1, 2, 3])
   */
  const areIdsValid = (idArray) => {
    return new Promise(async (resolve, reject) => {

      const selectedRoles = await db.role.findAndCountAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: idArray
          },
          isDeleted: false
        }
      })

      resolve({ result: idArray.length === selectedRoles.count })
    })
  }

  /**
   * If the name of a role is already taken, then it returns true. If you want a name available for adding a role, then you want false return.
   * @param {string} name
   * @returns {boolean}
   * @example 
   *  await roleValidation.isNameTaken("name")
   */
  const isNameTaken = (name) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingRole = await db.role.findOne({
        where: {
          name: name,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingRole !== null })
    })
  }


  /**
   * If the a permission is already attached to the role, then it returns true. If you want a permission available for adding a role, then you need a false return.
   * @param {string} rolePermissionIdObject - { permissionId, roleId } 
   * @returns {boolean}
   * @example 
   *  await roleValidation.doesRoleHavePermission({
   *    roleId,
   *    permissionId,
   *  })
   */
  const doesRoleHavePermission = ({ permissionId, roleId }) => {
    return new Promise(async (resolve, reject) => {
      const roleWithPermission = await db.roleManyPermission.findOne({
        where: {
          permissionId,
          roleId
        },
      });

      resolve({ result: roleWithPermission !== null })

    })
  }

  return {
    isIdValid,
    areIdsValid,
    isNameTaken,
    doesRoleHavePermission,
  }
}