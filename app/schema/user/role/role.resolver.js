const db = require("../../../models");
const makeRoleValidation = require("./role.validation")
const makeRoleLogic = require("./role.logic")
const makePermissionValidation = require("../permission/permission.validation")
const makePermissionLogic = require("../permission/permission.logic");
const { errorHandler } = require("../../utils");

const permissionValidation = makePermissionValidation(db)
const permissionLogic = makePermissionLogic(db)
const roleValidation = makeRoleValidation(db)
const roleLogic = makeRoleLogic(db)

const roleResolver = {
  Query: {
    role: async (parent, args) => {
      const user = await roleLogic.getOneById({
        id: args.id
      })

      return user;
      // return await db.role.findOne({ where: { id: args.id } });
    },
    roleMany: async (parent, args) => {
      try {
        const roles = await roleLogic.getManyWithPagination({
          q: args.q,
          page: args.page,
          pageSize: args.pageSize
        })

        if (roles.error) {
          return Error(roles.message)
        }

        return roles
      } catch (error) {
        errorHandler({ error })
      }
    },
  },
  RoleType: {
    permissionMany: async (parent, args) => {

      const permissions = await permissionLogic.getManyWithPagination({
        q: args.q,
        page: args.page,
        pageSize: args.pageSize,
        roleId: parent.id

      })

      return permissions
    },
  },
  Mutation: {
    roleAdd: async (parent, args) => {

      try {
        //is Rolename taken
        const isRoleNameTaken = await roleValidation.isNameTaken(args.name)
        if (isRoleNameTaken.result) {
          return Error("A role with that name already exist.");
        }

        //if permissions, are permissions valid?
        if (args.permissions) {
          const areIdsValid = await permissionValidation.areIdsValid(args.permissions.map(permission => permission.id))
          if (!areIdsValid.result) {
            return Error("A permission id is not valid.");
          }
        }

        const savedRole = await roleLogic.addOne({
          name: args.name
        })

        if (args.permissions) {

          const selectedPermissions = await roleLogic.addPermissionMany(args.permissions)
          //create relationship between new role and all permissions.
          // const newPermissionManyForRole = await db.roleManyPermission.bulkCreate(args.permissions.map(permission => ({
          //   roleId: savedRole.id,
          //   permissionId: permission.id,
          // })));
          // await newUserPermissionRole.save();

          savedRole.permissions = selectedPermissions.rows
        }

        return savedRole
      } catch (err) {
        console.log(err)
      }


    },
    roleUpdate: async (parent, args) => {

      //is Rolename taken
      const isRoleNameTaken = await roleValidation.isNameTaken(args.name)
      if (isRoleNameTaken.result) {
        return Error("A role with that name already exist.");
      }

      const role = await roleLogic.updateOne({
        id: args.id,
        name: args.name
      })

      return role ? role : Error("Nothing to update.");
    },
    roleDelete: async (parent, args) => {
      const role = await roleLogic.deleteOne({
        id: args.id
      })

      return role ? role : Error("Nothing to delete.")
    },
    roleAddPermission: async (parent, args) => {
      try {
        //does the permission exist
        const isIdValid = await permissionValidation.isIdValid(args.permissionId)
        if (!isIdValid.result) {
          return Error("Permission ID is not valid.")
        }

        const doesRoleHavePermission = await roleValidation.doesRoleHavePermission({
          roleId: args.roleId,
          permissionId: args.permissionId
        })

        if (doesRoleHavePermission.result) {
          return Error("This role already has this permission.")
        }

        await roleLogic.addPermissionOne({
          roleId: args.roleId,
          permissionId: args.permissionId
        })

        const permission = await permissionLogic.getOneById({
          id: args.permissionId
        })

        return permission

      } catch (err) {
        console.log(err)
      }
    },
    roleRemovePermission: async (parent, args) => {
      const permissionRemovedFromRole = await roleLogic.deletePermissionOne({
        roleId: args.roleId,
        permissionId: args.permissionId
      })

      const permission = await permissionLogic.getOneById({
        id: args.permissionId
      })

      return permission
    }
  },
};
module.exports = roleResolver;
