const makeUserValidation = require("./role.validation");
const mockedDB = require("../../../models/mocked")

const roleValidation = makeUserValidation(mockedDB);

test("Check role.validation isIdValid function.", async () => {
  const isIdValid = await roleValidation.isIdValid(1);

  expect(isIdValid.result).toBe(true)
})

test("Check role.validation areIdsValid function.", async () => {
  const areIdsValid = await roleValidation.areIdsValid([1, 2, 3000000000000]);

  expect(areIdsValid.result).toBe(false)
})

test("Check role.validation isNameTaken function.", async () => {
  const isNameTaken = await roleValidation.isNameTaken("testRole");

  expect(isNameTaken.result).toBe(false)
})

test("Check role.validation doesRoleHavePermission function.", async () => {
  const doesRoleHavePermission = await roleValidation.doesRoleHavePermission({permisisonId: 1, roleId: 1});

  expect(doesRoleHavePermission.result).toBe(true)
})
