const makeUserValidation = require("./userRequest.validation");
const mockedDB = require("../../../models/mocked")

const userRequestValidation = makeUserValidation(mockedDB);

test("Check userRequest.validation isIdValid function.", async () => {
  const isIdValid = await userRequestValidation.isIdValid(1);

  expect(isIdValid.result).toBe(true)
})

test("Check userRequest.validation areIdsValid function.", async () => {
  const areIdsValid = await userRequestValidation.areIdsValid([1, 2, 3000000000000]);

  expect(areIdsValid.result).toBe(false)
})

