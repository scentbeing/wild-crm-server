const makeUserRequestLogic = require("./userRequest.logic");
const mockedDB = require("../../../models/mocked")

const userRequestLogic = makeUserRequestLogic(mockedDB);

test("Check userRequest.logic getOneById function.", async () => {
  const userRequest = await userRequestLogic.getOneById({ id: 1 });
  expect(userRequest).toHaveProperty("email")
  expect(userRequest).toHaveProperty("username")
  expect(userRequest).toHaveProperty("name")
  expect(userRequest).toHaveProperty("status")
})

test("Check userRequest.logic getManyWithPagination function.", async () => {
  const userRequests = await userRequestLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20 });
  expect(userRequests.rows[0]).toHaveProperty("email")
  expect(userRequests.rows[0]).toHaveProperty("username")
  expect(userRequests.rows[0]).toHaveProperty("name")
  expect(userRequests.rows[0]).toHaveProperty("status")
})

test("Check userRequest.logic addOne", async () => {
  const userRequest = await userRequestLogic.addOne({ email: "test@example.com", username: "test", name: "test" });
  
  expect(userRequest).toHaveProperty('id')
  expect(userRequest).toHaveProperty("email")
  expect(userRequest).toHaveProperty("username")
  expect(userRequest).toHaveProperty("name")
})

test("Check userRequest.logic addMany", async () => {
  const userRequests = await userRequestLogic.addMany({userRequestNamesArray: [{ name: "userRequest1" }, {name: "userRequest2"}]});
  
  expect(userRequests[0].id).toBe(1)
})

test("Check userRequest.logic updateOne", async () => {
  const userRequest = await userRequestLogic.updateOne({id: 1, name: "asdf"});
  
  expect(userRequest.name).toMatch("asdf")
})

test("Check userRequest.logic deleteOne", async () => {
  const userRequest = await userRequestLogic.deleteOne({id: 1});
  
  expect(userRequest.isDeleted).toBe(true)
})
