const db = require("../../../models")
const makeUserRequestLogic = require("./userRequest.logic")
const makeUserLogic = require("../user/user.logic")
const makeAuthValidation = require("../auth/auth.validation")
const makeSettingRequestLogic = require("../../app/settings/request/settingRequest.logic")

const userRequestLogic = makeUserRequestLogic(db);
const userLogic = makeUserLogic(db)
const authValidation = makeAuthValidation(db)
const settingRequestLogic = makeSettingRequestLogic(db)

const userRequestResolver = {
  Query: {
    userRequest: async (parent, args) => {
      const userRequest = await userRequestLogic.getOneById({
        id: args.id
      })

      return userRequest
      // return await db.userRequest.findOne({ where: { id: args.id } });
    },
    userRequestMany: async (parent, args) => {
      const userRequests = await userRequestLogic.getManyWithPagination({
        q: args.q,
        page: args.page,
        pageSize: args.pageSize
      })

      if (userRequests.error) {
        return Error(userRequests.message)
      }

      return userRequests
    }
  },
  Mutation: {
    userRequestAdd: async (parent, args) => {
      const settingRequest = await settingRequestLogic.getOne();

      if (settingRequest.type === "MANUAL") {
        return Error("No user requests allowed at the moment.")

      }

      if (settingRequest.type === "REQUEST" && args.serverPassword !== settingRequest.password) {
        return Error("Server password is incorrect.")
        
      }

      const isPasswordValid = await authValidation.isPasswordValid(args.password);
      if (!isPasswordValid.result) {
        return Error(isPasswordValid.data)
      }

      const isEmailValid = await authValidation.isEmailValid(args.email);
      if (!isEmailValid.result) {
        return Error("Please use a proper formated email.");
      }

      const isEmailUnique = await authValidation.isEmailUnique(args.email);
      if (!isEmailUnique.result) {
        return Error("Please select a different email.")
      }
      const isUsernameUnique = await authValidation.isUsernameUnique(args.username)
      if (!isUsernameUnique.result) {
        return Error("Please select a different username.")

      }

      const userRequest = await userRequestLogic.addOne({
        email: args.email,
        password: args.password,
        username: args.username,
        name: args.name,
        status: args.status,
      })

      return userRequest
    },
    userRequestUpdate: async (parent, args) => {
      if (args.password) {
        const isPasswordValid = await authValidation.isPasswordValid(args.password);
        if (!isPasswordValid.result) {
          return Error(isPasswordValid.data)
        }
      }

      if (args.email) {
        const isEmailValid = await authValidation.isEmailValid(args.email);
        if (!isEmailValid.result) {
          return Error("Please use a proper formated email.");
        }
      }

      if (args.email) {
        const isEmailUnique = await authValidation.isEmailUnique(args.email);
        if (!isEmailUnique.result) {
          return Error("Please select a different email.")
        }
      }

      if (args.username) {
        const isUsernameUnique = await authValidation.isUsernameUnique(args.username)
        if (!isUsernameUnique.result) {
          return Error("Please select a different username.")
        }
      }

      const userRequest = await userRequestLogic.updateOne({
        email: args.email,
        password: args.password,
        username: args.username,
        name: args.name,
        status: args.status,
      })

      return userRequest ? userRequest : Error("Nothing to update.")
    },
    userRequestDelete: async (parent, args) => {
      const userRequest = await userRequestLogic.deleteOne({
        id: args.id
      })

      return userRequest ? userRequest : Error("Nothing to delete.")
    },
    userRequestApproved: async (parent, args) => {
      const userRequest = await userRequestLogic.getOneById({ id: args.id })

      const user = await userLogic.addOne({
        email: userRequest.email,
        overridePassword: userRequest.password,
        username: userRequest.username,
        profile: {
          name: userRequest.name
        }
      })

      const userRequestApproved = await userRequestLogic.approved({ id: args.id })

      return userRequestApproved ? userRequestApproved : Error("No request to denied.")
    },
    userRequestDenied: async (parent, args) => {
      const userRequest = await userRequestLogic.denied({ id: args.id })

      return userRequest ? userRequest : Error("No request to denied.")
    },
  },
};
module.exports = userRequestResolver;
