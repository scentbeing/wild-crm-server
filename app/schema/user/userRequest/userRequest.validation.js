/**
 * The functions responsible for validating the userRequest type.
 * @module userRequest_validation
 */

module.exports = (db) => {

  /**
   * Check validation of the userRequestId. It will return true if the userRequestId is real.
   * @param {number} id
   * @returns {boolean}
   * @example 
   *  await userRequestValidation.isIdValid(id)
   */
  const isIdValid = (id) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingUserRequest = await db.userRequest.findOne({
        where: {
          id,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingUserRequest !== null })
    });
  }

  /**
   * Check validation of many userRequestIds. It will return true if all ids are true.
   * @param {Array<number>} idArray
   * @returns {boolean}
   * @example 
   *  await userRequestValidation.areIdsValid([1, 2, 3])
   */
  const areIdsValid = (idArray) => {
    return new Promise(async (resolve, reject) => {

      const selectedUserRequests = await db.userRequest.findAndCountAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: idArray
          },
          isDeleted: false
        }
      })

      resolve({ result: idArray.length === selectedUserRequests.count })
    })
  }

  return {
    isIdValid,
    areIdsValid,
  }
}
