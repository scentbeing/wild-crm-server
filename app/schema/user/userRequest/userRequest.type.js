const gql = require("graphql-tag");
const { paginationType } = require("../../utils");

const applicationType = gql`

  ${paginationType("UserRequestPaginationType", "UserRequestType")}

  enum userRequestStatus {
    PENDING
    ACCEPTED
    REJECTED
  }

  type UserRequestType {
    id: ID
    email: String,
    password: String,
    username: String,
    name: String,
    status: userRequestStatus
  }

  input UserRequestInput {
    id: ID
    email: String,
    password: String,
    username: String,
    name: String,
    status: userRequestStatus
  }
  
  type Query {
    userRequest(id: ID): UserRequestType
    userRequestMany(q: String, page: Int, pageSize: Int): UserRequestPaginationType
  }
  type Mutation {
    userRequestAdd(email: String!, password: String!, username: String!, name: String!, serverPassword: String): UserRequestType
    userRequestUpdate(id: Int!, email: String, password: String, username: String, name: String): UserRequestType
    userRequestDelete(id: Int!): UserRequestType

    userRequestApproved(id: Int!) : UserRequestType
    userRequestDenied(id: Int!) : UserRequestType
  }
`;
module.exports = applicationType;
