const bcrypt = require("bcryptjs");

/**
 * The functions responsible for handling the userRequest type.
 * @module userRequest_logic
 */

module.exports = (db) => {
  const Op = db.Sequelize.Op;

  /**
   * Find a userRequest by Id.
   * @param {Object} idObject - {id: 1} 
   * @returns {userRequest}
   * @example 
   *  await userRequestLogic.getOneById({
   *    id
   *  })
   */
  const getOneById = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const userRequest = await db.userRequest.findOne({ where: { id, isDeleted: false } });

      resolve(userRequest.dataValues)
    })
  }

  /**
   * Find many userRequests with pagination.
   * @param {searchParam} paginationObject - {q, page, pageSize}
   * @returns {pagination<userRequest>}
   * @example 
   *  await userRequestLogic.getManyWithPagination({
   *    q: "search",
   *    page: 2,
   *    pageSize:2
   *  })
   */
  const getManyWithPagination = ({ q, page, pageSize }) => {
    return new Promise(async (resolve, reject) => {
      page = page ? page - 1 : 0;
      pageSize = pageSize || 10;

      if (page < 0) {
        resolve({ error: true, message: new Error("Please start the page at 1.") });
      }
      if (pageSize < 0 || pageSize >= 100) {
        resolve({ error: true, message: new Error("Please keep pageSize inbetween 1 - 100.") });
      }

      const offset = page * pageSize;
      const limit = pageSize;

      let search = {
        where: {
          isDeleted: false,
        },
      };

      if (q) {
        search = Object.assign(search, {
          where: {
            name: {
              [Op.like]: "%" + q + "%",
            },
          },
        });
      }

      search.offset = offset;
      search.limit = limit;

      const userRequests = await db.userRequest.findAndCountAll(search);
      userRequests.page = page + 1;
      userRequests.pageSize = pageSize;
      userRequests.pageCount = Math.ceil(
        userRequests.count / userRequests.pageSize
      );
      userRequests.rows = userRequests.rows.map(p => p.dataValues)

      resolve(userRequests)
    })
  }

  /**
   * Save a userRequest.
   * @param {userRequest} userRequestObject - { email, password, username, name, status } It takes all the "userRequest" properties except id.
   * @returns {userRequest} - It returns the same object but with an id.
   * @example 
   *  await userRequestLogic.addOne({
   *    email: args.email,
   *    password: args.password
   *    username: args.username,
   *    name: args.name,
   *    status: args.status,
   *  })
   */
  const addOne = ({ email, password, username, name, status, }) => {
    return new Promise(async (resolve, reject) => {

      const newUserRequest = db.userRequest.build({
        email,
        password: password ? bcrypt.hashSync(password, 8) : undefined,
        username,
        name,
        status
      });

      resolve(await newUserRequest.save())
    })
  }

  /**
   * Save many userRequests.
   * @param {Array<userRequests>} ArrayOfUserRequests - [{ email, username, name, status }] The array ojbect takes all the "userRequest" properties except id.
   * @returns {boolean} - The result is true or false for the completion of the saves.
   * @example 
   *  await userRequestLogic.addMany([{
   *    email,
   *    password,
   *    username,
   *    name,
   *    status,
   *  }])
   */
  const addMany = ({ userRequestNamesArray }) => {
    return new Promise(async (resolve, reject) => {

      const newUserRequests = await db.userRequest.bulkCreate(userRequestNamesArray);

      resolve(newUserRequests)
    })
  }

  /**
   * Update a userRequest.
   * @param {userRequest} userRequestObject - { id, email, password, username, name, status } It takes all the "role" properties. Id is required.
   * @returns {userRequest} 
   * @example 
   *  await userRequestLogic.updateOne([{
   *    id,
   *    email,
   *    password,
   *    username,
   *    name,
   *    status,
   *  }])
   */
  const updateOne = ({ id, email, password, username, name, status }) => {
    return new Promise(async (resolve, reject) => {

      const userRequest = await db.userRequest.update(
        {
          email,
          password: password ? bcrypt.hashSync(password, 8) : undefined,
          username,
          name,
          status
        },
        {
          where: { id: id, isDeleted: false },
          returning: true,
          plain: true,
        }
      );

      resolve(userRequest[0] !== 0 ? userRequest[1][0].dataValues : null)
    })
  }

  /**
   * Delete a userRequest. A soft delete from the column "is_deleted" becoming true.
   * @param {Object} idObject - { id } Id is required.
   * @returns {userRequest} 
   * @example 
   *  await userRequestLogic.deleteOne({
   *    id,
   *  })
   */
  const deleteOne = ({ id }) => {
    return new Promise(async (resolve, reject) => {

      const userRequest = await db.userRequest.update(
        { isDeleted: true },
        {
          where: { id: id, isDeleted: false },
          returning: true,
          plain: true,
        }
      );

      resolve(userRequest[0] !== 0 ? userRequest[1][0].dataValues : null)
    })
  }

  /**
   * Approve a userRequest.
   * @param {Object} idObject - { id } Id is required.
   * @returns {userRequest} 
   * @example 
   *  await userRequestLogic.approved({
   *    id,
   *  })
   */
  const approved = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const userRequest = await db.userRequest.update(
        {
          status: "ACCEPTED"
        },
        {
          where: { id: id, isDeleted: false },
          returning: true,
        }
      );

      resolve(userRequest[0] !== 0 ? userRequest[1][0].dataValues : null)

    })
  }

  /**
   * Deny a userRequest.
   * @param {Object} idObject - { id } Id is required.
   * @returns {userRequest} 
   * @example 
   *  await userRequestLogic.denied({
   *    id,
   *  })
   */
  const denied = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const userRequest = await db.userRequest.update(
        {
          status: "REJECTED"
        },
        {
          where: { id: id, isDeleted: false },
          returning: true,
        }
      );

      resolve(userRequest[0] !== 0 ? userRequest[1][0].dataValues : null)

    })
  }

  return {
    getOneById,
    getManyWithPagination,
    addOne,
    addMany,
    updateOne,
    deleteOne,

    approved,
    denied
  }

}


