const errorHandler = ({ error }) => {
    return new Promise((resolve, reject) => {
        console.log(error)

        resolve({ success: true })
    })
}

module.exports = {
    errorHandler,
}