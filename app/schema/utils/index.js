const {paginationType} = require("./paginationType")
const {errorHandler} = require("./errorHandler")

module.exports = {
    paginationType,
    errorHandler
}