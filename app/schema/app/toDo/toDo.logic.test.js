const makeToDoLogic = require("./toDo.logic");
const mockedDB = require("../../../models/mocked")

const toDoLogic = makeToDoLogic(mockedDB);

test("Check toDo.logic getOneById function.", async () => {
  const toDo = await toDoLogic.getOneById({ id: 1 });
  expect(toDo).toMatchObject({ id: 1, title: "first to do" })
})

test("Check toDo.logic getManyWithPagination function.", async () => {
  const toDos = await toDoLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20 });
  expect(toDos.rows[0]).toHaveProperty("title")
})

test("Check toDo.logic getManyWithPagination association with toDoList.", async () => {
  const toDos = await toDoLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20, toDoListId: 1 });
  expect(toDos.rows[0]).toHaveProperty("title")
})

test("Check toDo.logic addOne", async () => {
  const toDo = await toDoLogic.addOne({ title: "test" });

  expect(toDo.title).toMatch("test")
  expect(toDo).toHaveProperty('id')
})

test("Check toDo.logic addMany", async () => {
  const toDos = await toDoLogic.addMany({ toDoNamesArray: [{ title: "toDo1" }, { title: "toDo2" }] });

  expect(toDos[0].id).toBe(1)
})

// test("Check toDo.logic updateOne", async () => {
//   const toDo = await toDoLogic.updateOne({ id: 1, title: "asdf" });

//   expect(toDo.title).toMatch("asdf")
// })

test("Check toDo.logic deleteOne", async () => {
  const toDo = await toDoLogic.deleteOne({ id: 1, name: "delete test" });

  expect(toDo.isDeleted).toBe(true)
})
