const gql = require("graphql-tag");
const { paginationType } = require("../../utils");

const applicationType = gql`

  ${paginationType("ToDoPaginationType", "ToDoType")}

  type ToDoType {
    id: ID
    title: String
    dueDate: String
    isCompleted: Boolean
  }

  input ToDoInput {
    id: ID
    title: String
    dueDate: String
    isCompleted: Boolean
  }
  
  # type Query {
  #   toDo(id: ID): ToDoType
  #   toDoMany(q: String, page: Int, pageSize: Int): ToDoPaginationType
  # }
  # type Mutation {
  #   toDoAdd(title: String!, dueDate: String, isCompleted: Boolean): ToDoType
  #   toDoUpdate(name: String!, title: String, dueDate: String, isCompleted: Boolean): ToDoType
  #   toDoDelete(id: Int!): ToDoType
  # }
`;
module.exports = applicationType;
