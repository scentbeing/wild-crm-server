/**
 * The functions responsible for validating the toDo type.
 * @module toDo_validation
 */

module.exports = (db) => {

  /**
   * Check validation of the toDoId. It will return true if the toDoId is real.
   * @param {number} id
   * @returns {boolean}
   * @example 
   *  await toDoValidation.isIdValid(id)
   */
  const isIdValid = (id) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingToDo = await db.toDo.findOne({
        where: {
          id,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingToDo !== null })
    });
  }

  /**
   * Check validation of many toDoIds. It will return true if all ids are true.
   * @param {Array<number>} idArray
   * @returns {boolean}
   * @example 
   *  await toDoValidation.areIdsValid([1, 2, 3])
   */
  const areIdsValid = (idArray) => {
    return new Promise(async (resolve, reject) => {

      const selectedToDos = await db.toDo.findAndCountAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: idArray
          },
          isDeleted: false
        }
      })

      resolve({ result: idArray.length === selectedToDos.count })
    })
  }

  /**
   * If the name of a toDo is already taken, then it returns true. If you want a name available for adding a toDo, then you want false return.
   * @param {string} name
   * @returns {boolean}
   * @example 
   *  await toDoValidation.isNameTaken("name")
   */
  const isTitleTaken = (title) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingToDo = await db.toDo.findOne({
        where: {
          title,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingToDo !== null })
    })
  }

  return {
    isIdValid,
    areIdsValid,
    isTitleTaken,
  }
}
