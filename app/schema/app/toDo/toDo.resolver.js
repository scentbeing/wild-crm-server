const db = require("../../../models")
const makeToDoLogic = require("./toDo.logic")
const makeToDoValidation = require("./toDo.validation")

const toDoLogic = makeToDoLogic(db);
const toDoValidation = makeToDoValidation(db)

const toDoResolver = {
  Query: {
    // toDo: async (parent, args) => {
    //   const toDo = await toDoLogic.getOneById({
    //     id: args.id
    //   })

    //   return toDo
    //   // return await db.toDo.findOne({ where: { id: args.id } });
    // },
    // toDoMany: async (parent, args) => {
    //   const toDos = await toDoLogic.getManyWithPagination({
    //     q: args.q,
    //     page: args.page,
    //     pageSize: args.pageSize
    //   })

    //   if (toDos.error) {
    //     return Error(toDos.message)
    //   }

    //   return toDos
    // }
  },
  Mutation: {
    // toDoAdd: async (parent, args) => {
    //   const isToDoNameTaken = await toDoValidation.isNameTaken({
    //     name: args.name
    //   })

    //   if (isToDoNameTaken.result) {
    //     return Error("A toDo with that name already exist.");
    //   }

    //   const toDo = await toDoLogic.addOne({
    //     name: args.name
    //   })

    //   return toDo
    // },
    // toDoUpdate: async (parent, args) => {
    //   const isToDoNameTaken = await toDoValidation.isNameTaken({
    //     name: args.name
    //   })

    //   if (isToDoNameTaken.result) {
    //     return Error("A toDo with that name already exist.");
    //   }

    //   const toDo = await toDoLogic.updateOne({
    //     id: args.id,
    //     name: args.name
    //   })

    //   return toDo ? toDo : Error("Nothing to update.")
    // },
    // toDoDelete: async (parent, args) => {
    //   const toDo = await toDoLogic.deleteOne({
    //     id: args.id
    //   })

    //   return toDo ? toDo : Error("Nothing to delete.")
    // },
  },
};
module.exports = toDoResolver;
