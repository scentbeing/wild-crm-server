/**
 * The functions responsible for handling the toDo type.
 * @module toDo_logic
 */

module.exports = (db) => {
  const Op = db.Sequelize.Op;

  /**
   * Find a toDo by Id.
   * @param {Object} idObject - {id: 1} 
   * @returns {toDo}
   * @example 
   *  await toDoLogic.getOneById({
   *    id
   *  })
   */
  const getOneById = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const toDo = await db.toDo.findOne({ where: { id, isDeleted: false } });

      resolve(toDo.dataValues)
    })
  }

  /**
   * Find many toDos with pagination.
   * @param {searchParam} paginationObject - {q, page, pageSize}
   * @returns {pagination<toDo>}
   * @example 
   *  await toDoLogic.getManyWithPagination({
   *    q: "search",
   *    page: 2,
   *    pageSize:2
   *  })
   */
  const getManyWithPagination = ({ q, page, pageSize, userId, toDoListId }) => {
    return new Promise(async (resolve, reject) => {
      page = page ? page - 1 : 0;
      pageSize = pageSize || 10;

      if (page < 0) {
        resolve({ error: true, message: new Error("Please start the page at 1.") });
      }
      if (pageSize < 0 || pageSize >= 100) {
        resolve({ error: true, message: new Error("Please keep pageSize inbetween 1 - 100.") });
      }

      const offset = page * pageSize;
      const limit = pageSize;

      let search = {
        where: {
          toDoListId,
          isDeleted: false,
        },
      };

      if (userId) {
        search.where.userId = userId;
      }

      if (q) {
        search.where = Object.assign(search.where, {

          title: {
            [Op.like]: "%" + q + "%",
          },

        });
      }

      search.offset = offset;
      search.limit = limit;

      const toDos = await db.toDo.findAndCountAll(search);

      if (toDos) {
        toDos.page = page + 1;
        toDos.pageSize = pageSize;
        toDos.pageCount = Math.ceil(
          toDos.count / toDos.pageSize
        );
        toDos.rows = toDos.rows.map(p => p.dataValues)
      }

      resolve(toDos)
    })
  }

  /**
   * Save a toDo.
   * @param {toDo} toDoObject - { title, dueDate, isCompleted } It takes all the "toDo" properties except id.
   * @returns {toDo} - It returns the same object but with an id.
   * @example 
   *  await toDoLogic.addOne({
   *    title: "title",
   *    dueDate: "dueDate",
   *    isCompleted: boolean
   *  })
   */
  const addOne = ({ toDoListId, title, dueDate, isCompleted }) => {
    return new Promise(async (resolve, reject) => {

      const newToDo = db.toDo.build({ toDoListId, title, dueDate, isCompleted });

      resolve(await newToDo.save())
    })
  }

  /**
   * Save many toDos.
   * @param {Array<toDos>} ArrayOfToDos - [{ title, dueDate, isCompleted }] The array ojbect takes all the "toDo" properties except id.
   * @returns {boolean} - The result is true or false for the completion of the saves.
   * @example 
   *  await toDoLogic.addMany([{
   *    title: "title",
   *    dueDate: "dueDate",
   *    isCompleted: boolean
   *  }])
   */
  const addMany = (toDoNamesArray) => {
    return new Promise(async (resolve, reject) => {

      const newToDos = await db.toDo.bulkCreate(toDoNamesArray);

      resolve(newToDos)
    })
  }

  /**
   * Update a toDo.
   * @param {toDo} toDoObject - { id, title, dueDate, isCompleted } It takes all the "role" properties. Id is required.
   * @returns {toDo} 
   * @example 
   *  await toDoLogic.updateOne([{
   *    id,
   *    title: "title",
   *    dueDate: "dueDate",
   *    isCompleted: boolean
   *  }])
   */
  const updateOne = ({ id, title, dueDate, isCompleted }) => {
    return new Promise(async (resolve, reject) => {

      const toDo = await db.toDo.update(
        { title, dueDate, isCompleted },
        {
          where: { id: id, isDeleted: false },
          returning: true,
          plain: true,
        }
      );

      resolve(toDo[0] !== 0 ? toDo[1].dataValues : null)
    })
  }

  /**
   * Delete a toDo. A soft delete from the column "is_deleted" becoming true.
   * @param {Object} idObject - { id } Id is required.
   * @returns {toDo} 
   * @example 
   *  await toDoLogic.deleteOne({
   *    id,
   *  })
   */
  const deleteOne = ({ id }) => {
    return new Promise(async (resolve, reject) => {

      const toDo = await db.toDo.update(
        { isDeleted: true },
        {
          where: { id, isDeleted: false},
          returning: true,
        }
      );

      resolve(toDo[0] !== 0 ? toDo[1][0].dataValues : null)
    })
  }


  return {
    getOneById,
    getManyWithPagination,
    addOne,
    addMany,
    updateOne,
    deleteOne,
  }

}


