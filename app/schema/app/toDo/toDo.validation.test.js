const makeUserValidation = require("./toDo.validation");
const mockedDB = require("../../../models/mocked")

const toDoValidation = makeUserValidation(mockedDB);

test("Check toDo.validation isIdValid function.", async () => {
  const isIdValid = await toDoValidation.isIdValid(1);

  expect(isIdValid.result).toBe(true)
})

test("Check toDo.validation areIdsValid function.", async () => {
  const areIdsValid = await toDoValidation.areIdsValid([1, 2, 3000000000000]);

  expect(areIdsValid.result).toBe(false)
})

test("Check toDo.validation isTitleTaken function.", async () => {
  const isTitleTaken = await toDoValidation.isTitleTaken("user:manage");

  expect(isTitleTaken.result).toBe(false)
})
