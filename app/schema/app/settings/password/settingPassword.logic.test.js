const makeSettingPasswordLogic = require("./settingPassword.logic")
const mockedDB = require("../../../../models/mocked")

const settingPasswordLogic = makeSettingPasswordLogic(mockedDB)

test("Check settingPassword.logic getOne function.", async () => {
    const settingPassword = await settingPasswordLogic.getOne()
    expect(settingPassword).toHaveProperty("passwordLength")
    expect(settingPassword).toHaveProperty("shouldHaveUppercaseLetter")
    expect(settingPassword).toHaveProperty("shouldHaveLowercaseLetter")
    expect(settingPassword).toHaveProperty("shouldHaveNumber")
    expect(settingPassword).toHaveProperty("shouldHaveSymbol")
})

test("Check settingPassword.logic updateOne function.", async () => {
    const settingPassword = await settingPasswordLogic.updateOne({ passwordLength: 10 })
    expect(settingPassword).toHaveProperty("passwordLength")
    expect(settingPassword).toHaveProperty("shouldHaveUppercaseLetter")
    expect(settingPassword).toHaveProperty("shouldHaveLowercaseLetter")
    expect(settingPassword).toHaveProperty("shouldHaveNumber")
    expect(settingPassword).toHaveProperty("shouldHaveSymbol")
})
