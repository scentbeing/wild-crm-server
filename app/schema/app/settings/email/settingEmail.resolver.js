const db = require("../../../../models")
const makeSettingEmailLogic = require("./settingEmail.logic")

const settingEmailLogic = makeSettingEmailLogic(db)

const settingEmailResolver = {
  Query: {
    settingEmail: async (parent, args) => {

      const settingEmail = await settingEmailLogic.getOne()

      return settingEmail
    },
  },
  Mutation: {
    settingEmailUpdate: async (parent, args) => {

      const settingEmail = await settingEmailLogic.updateOne({
        emailVerificationSubject: args.emailVerificationSubject,
        emailVerificationMessage: args.emailVerificationMessage,
        passwordResetSubject: args.passwordResetSubject,
        passwordResetMessage: args.passwordResetMessage,
        resetPasswordEmailSubject: args.resetPasswordEmailSubject,
        resetPasswordEmailMessage: args.resetPasswordEmailMessage,
        inviteUserSubject: args.inviteUserSubject,
        inviteUserMessage: args.inviteUserMessage,
      });

      return settingEmail;
    },
  },
};

module.exports = settingEmailResolver;
