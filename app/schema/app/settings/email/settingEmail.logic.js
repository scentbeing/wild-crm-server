const bcrypt = require("bcryptjs");

/**
 * The functions responsible for handling the settingEmail type.
 * @module settingEmail_logic
 */


module.exports = (db) => {
  /**
   * Retrieves the email settings. 
   * @returns {settingEmail}
   * @example 
   *  await settingEmailLogic.getOne()
   * 
   */
  const getOne = () => {
    return new Promise(async (resolve, reject) => {

      const settingEmail = await db.settingEmail.findOne();

      resolve(settingEmail)
    })
  }

  /**
   * Updates the email settings
   * @param {Object} settingEmailObject - { emailVerificationSubject, emailVerificationMessage, passwordResetSubject, passwordResetMessage, resetPasswordEmailSubject, resetPasswordEmailMessage, inviteUserSubject, inviteUserMessage }
   * @returns {settingEmail}
   * @example 
   *  await settingEmailLogic.updateOne({
   *    emailVerificationSubject,
   *    emailVerificationMessage,
   *    passwordResetSubject,
   *    passwordResetMessage,
   *    resetPasswordEmailSubject,
   *    resetPasswordEmailMessage,
   *    inviteUserSubject,
   *    inviteUserMessage
   *  })
   * 
   */

  const updateOne = ({ emailVerificationSubject, emailVerificationMessage, passwordResetSubject, passwordResetMessage, resetPasswordEmailSubject, resetPasswordEmailMessage, inviteUserSubject, inviteUserMessage }) => {
    return new Promise(async (resolve, reject) => {

      const settingEmail = await db.settingEmail.findOne();

      await settingEmail.update({
        emailVerificationSubject,
        emailVerificationMessage,
        passwordResetSubject,
        passwordResetMessage,
        resetPasswordEmailSubject,
        resetPasswordEmailMessage,
        inviteUserSubject,
        inviteUserMessage,
      })

      resolve(settingEmail)
    })
  }

  return {
    getOne,
    updateOne,
  }
}