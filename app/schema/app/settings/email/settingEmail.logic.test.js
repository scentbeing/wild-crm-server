const makeSettingEmailLogic = require("./settingEmail.logic")
const mockedDB = require("../../../../models/mocked")

const settingEmailLogic = makeSettingEmailLogic(mockedDB)

test("Check settingEmail.logic getOne function.", async () => {
  const settingEmail = await settingEmailLogic.getOne()
  expect(settingEmail).toHaveProperty("emailVerificationSubject")
  expect(settingEmail).toHaveProperty("emailVerificationMessage")
  expect(settingEmail).toHaveProperty("passwordResetSubject")
  expect(settingEmail).toHaveProperty("passwordResetMessage")
  expect(settingEmail).toHaveProperty("resetPasswordEmailSubject")
  expect(settingEmail).toHaveProperty("resetPasswordEmailMessage")
  expect(settingEmail).toHaveProperty("inviteUserSubject")
})

test("Check settingEmail.logic updateOne function.", async () => {
  const settingEmail = await settingEmailLogic.updateOne({
    emailVerificationSubject: "test email subject"
  })
  expect(settingEmail).toHaveProperty("emailVerificationSubject")
  expect(settingEmail).toHaveProperty("emailVerificationMessage")
  expect(settingEmail).toHaveProperty("passwordResetSubject")
  expect(settingEmail).toHaveProperty("passwordResetMessage")
  expect(settingEmail).toHaveProperty("resetPasswordEmailSubject")
  expect(settingEmail).toHaveProperty("resetPasswordEmailMessage")
  expect(settingEmail).toHaveProperty("inviteUserSubject")
  expect(settingEmail).toHaveProperty("inviteUserMessage")
})
