const db = require("../../../../models")
const makeSettingGeneralLogic = require('./settingGeneral.logic')

const settingGeneralLogic = makeSettingGeneralLogic(db);

const settingGeneralResolver = {
  Query: {
    settingGeneral: async (parent, args) => {
      
      const settingGeneral = await settingGeneralLogic.getOne()

      return settingGeneral
    },
  },
  Mutation: {
    settingGeneralUpdate: async (parent, args) => {
      
      const settingGeneral = await settingGeneralLogic.updateOne({
        companyName: args.companyName,
        address1: args.address1,
        address2: args.address2,
        address3: args.address3,
        address4: args.address4,
        city: args.city,
        country: args.country,
        postal: args.postal,
        phone: args.phone,        
      })
      
      return settingGeneral;
    },
  },
};
module.exports = settingGeneralResolver;
