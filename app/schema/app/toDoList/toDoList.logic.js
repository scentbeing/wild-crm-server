/**
 * The functions responsible for handling the user type.
 * @module toDoList_logic
 */

module.exports = (db) => {
  const Op = db.Sequelize.Op;
  /**
   * Find a toDoList by Id.
   * @param {Object} idObject - {id: 1} 
   * @returns {toDoList}
   * @example 
   *  await toDoListLogic.getOneById({
   *    id
   *  })
   */
  const getOneById = ({ id }) => {
    return new Promise(async (resolve, reject) => {
      const record = await db.toDoList.findOne({ where: { id: id, isDeleted: false } });

      resolve(record);
    });
  }

  /**
   * Find many toDoLists with pagination.
   * @param {searchParam} paginationObject - {q, page, pageSize}
   * @returns {pagination<toDoList>}
   * @example 
   *  await toDoListLogic.getManyWithPagination({
   *    q: "search",
   *    page: 2,
   *    pageSize:2,
   *    //filter for user
   *    userId,
   *  })
   */
  const getManyWithPagination = ({ q, page, pageSize, userId }) => {
    return new Promise(async (resolve, reject) => {

      page = page ? page - 1 : 0;
      pageSize = pageSize || 10;

      if (page < 0) {
        return resolve({ error: true, humanReadable: true, text: "Please start the page at 1." });
      }

      if (pageSize < 0 || pageSize >= 100) {
        return resolve({ error: true, humanReadable: true, text: "Please keep pageSize inbetween 1 - 100." });
      }

      const offset = page * pageSize;
      const limit = pageSize;

      let search = {
        where: {
          isDeleted: false,
        },
      };

      if (userId) {
        search.where.userId = userId;
      }

      if (q) {
        search = {
          where: {
            name: {
              [Op.like]: "%" + q + "%",
            },
            isDeleted: false,
          },
        };
      }

      search.offset = offset;
      search.limit = limit;

      const toDoLists = await db.toDoList.findAndCountAll(search);

      if (toDoLists) {
        toDoLists.page = page + 1;
        toDoLists.pageSize = pageSize;
        toDoLists.pageCount = Math.ceil(toDoLists.count / toDoLists.pageSize);
      }

      resolve(toDoLists);
    });
  }


  /**
   * Save a toDoList. This includes the permissions.
   * @param {toDoList} toDoListObject - { name, toDoListMany } It takes all the "user" properties except id.
   * @returns {toDoList} - It returns the same object but with an id.
   * @example 
   *  await toDoListLogic.addOne({
   *    name,
   *    userId,
   *  })
   */
  const addOne = ({ name, userId }) => {
    return new Promise(async (resolve, reject) => {

      const newToDoList = db.toDoList.build({
        name,
        userId,
      });

      resolve(await newToDoList.save())
    })
  }

  /**
   * Save many toDoLists. This includes the permissions.
   * @param {Array<toDoList>} ArrayOfToDoLists - [{ name, toDoListMany }] It takes all the "toDoList" properties except id.
   * @returns {boolean} - The result is true or false for the completion of the saves.
   * @example 
   *  await toDoListLogic.addMany([{
   *    name,
   *    userId,
   *  }])
   */
  const addMany = ({ toDoListNamesArray }) => {
    return new Promise(async (resolve, reject) => {

      const newToDoLists = await db.toDoList.bulkCreate(toDoListNamesArray.map(toDoLists => ({
        name: toDoLists.name,
        userId: toDoLists.userId
      })));

      resolve(newToDoLists)
    })
  }

  /**
   * Update a toDoList. There is no functionality for updating permissions here.
   * @param {toDoList} toDoListObject - { id, name } It takes all the "toDoList" properties. Id is required.
   * @returns {toDoList} 
   * @example 
   *  await toDoListLogic.updateOne([{
   *    id,
   *    name,
   *    userId,
   *  }])
   */
  const updateOne = ({ id, name, userId }) => {
    return new Promise(async (resolve, reject) => {

      const toDoList = await db.toDoList.update(
        {
          name,
          userId,
        },
        {
          where: { id: id, isDeleted: false },
          returning: true,
        }
      );

      resolve(toDoList[0] !== 0 ? toDoList[1][0].dataValues : null)
    })
  }

  /**
   * Delete a toDoList. A soft delete from the column "is_deleted" becoming true.
   * @param {Object} idObject - { id } Id is required.
   * @returns {user} 
   * @example 
   *  await toDoListLogic.deleteOne({
   *    id,
   *  })
   */
  const deleteOne = ({ id }) => {
    return new Promise(async (resolve, reject) => {

      const toDoList = await db.toDoList.update(
        { isDeleted: true },
        {
          where: { id, isDeleted: false },
          returning: true,
          // plain: true,
        }
      );

      resolve(toDoList[0] !== 0 ? toDoList[1][0].dataValues : null)
    })
  }

  return {
    // self
    getOneById,
    getManyWithPagination,
    addOne,
    addMany,
    updateOne,
    deleteOne
  }

}

