const db = require("../../../models");
const makeToDoListValidation = require("./toDoList.validation")
const makeToDoListLogic = require("./toDoList.logic")
const makePermissionValidation = require("../../user/permission/permission.validation")
const makePermissionLogic = require("../../user/permission/permission.logic");
const { errorHandler } = require("../../utils");
const makeToDoLogic = require("../toDo/toDo.logic");

const permissionValidation = makePermissionValidation(db)
const permissionLogic = makePermissionLogic(db)
const toDoListValidation = makeToDoListValidation(db)
const toDoListLogic = makeToDoListLogic(db)
const toDoLogic = makeToDoLogic(db)

const toDoListResolver = {
  Query: {
    toDoList: async (parent, args) => {
      const user = await toDoListLogic.getOneById({
        id: args.id
      })

      return user;
      // return await db.toDoList.findOne({ where: { id: args.id } });
    },
    toDoListMany: async (parent, args, ctx) => {
      try {
        const searchObj = {
          q: args.q,
          page: args.page,
          pageSize: args.pageSize
        }

        if (ctx.user?.roleMany?.rows.filter(role => role.name === "admin")) {
          searchObj.userId = ctx.user.id
        }

        const toDoLists = await toDoListLogic.getManyWithPagination(searchObj)

        if (toDoLists.error) {
          return Error(toDoLists.message)
        }

        return toDoLists
      } catch (error) {
        errorHandler({ error })
      }
    },
  },
  ToDoListType: {
    toDoMany: async (parent, args, ctx) => {

      const searchObj = {
        q: args.q,
        page: args.page,
        pageSize: args.pageSize,
        toDoListId: parent.id
      };

      const toDos = await toDoLogic.getManyWithPagination(searchObj)

      return toDos
    },
  },
  Mutation: {
    toDoListAdd: async (parent, args, ctx) => {

      try {
        //is ToDoListname taken
        const isToDoListNameTaken = await toDoListValidation.isNameTaken(args.name)
        if (isToDoListNameTaken.result) {
          return Error("A toDoList with that name already exist.");
        }

        const savedToDoList = await toDoListLogic.addOne({
          name: args.name,
          userId: ctx.user.id
        })

        return savedToDoList
      } catch (err) {
        console.log(err)
      }
    },
    toDoListUpdate: async (parent, args) => {

      //is ToDoListname taken
      const isToDoListNameTaken = await toDoListValidation.isNameTaken(args.name)
      if (isToDoListNameTaken.result) {
        return Error("A toDoList with that name already exist.");
      }

      const toDoList = await toDoListLogic.updateOne({
        id: args.id,
        name: args.name
      })

      return toDoList ? toDoList : Error("Nothing to update.");
    },
    toDoListDelete: async (parent, args) => {
      const toDoList = await toDoListLogic.deleteOne({
        id: args.id
      })

      return toDoList ? toDoList : Error("Nothing to delete.")
    },
    toDoListAddToDo: async (parent, args) => {
      try {
        const toDo = await toDoLogic.addOne({
          title: args.title,
          isCompleted: args.isCompleted,
          dueDate: args.dueDate,
          toDoListId: args.toDoListId
        })

        return toDo

      } catch (err) {
        console.log(err)
      }
    },
    toDoListUpdateToDo: async (parent, args) => {
        const toDo = await toDoLogic.updateOne({
          id: args.toDoId,
          title: args.title,
          isCompleted: args.isCompleted,
          dueDate: args.dueDate,
        })
  
        return toDo ? toDo : Error("Nothing to update.")

    },
    toDoListRemoveToDo: async (parent, args) => {
        const toDo = await toDoLogic.deleteOne({
          id: args.toDoId
        })
  
        return toDo ? toDo : Error("Nothing to delete.")
    }
  },
};
module.exports = toDoListResolver;
