const gql = require("graphql-tag");
const { paginationType } = require("../../utils");

const toDoListType = gql`

  ${paginationType("ToDoListPaginationType", "ToDoListType")}

  type ToDoListType {
    id: ID
    name: String
    toDoMany(q: String, page: Int, pageSize: Int) : ToDoPaginationType
  }

  input ToDoListInput {
    id: ID
    name: String
    toDoMany: [ToDoInput]
  }
  
  type Query {
    toDoList(id: ID): ToDoListType
    toDoListMany(q: String, page: Int, pageSize: Int): ToDoListPaginationType
  }
  type Mutation {
    toDoListAdd(name: String!): ToDoListType
    toDoListUpdate(id: Int!, name: String!): ToDoListType
    toDoListDelete(id: Int!): ToDoListType

    
    toDoListAddToDo(toDoListId: Int!, title: String!, dueDate: String, isCompleted: Boolean) : ToDoType
    toDoListUpdateToDo(toDoId: Int!, title: String, dueDate: String, isCompleted: Boolean) : ToDoType
    toDoListRemoveToDo(toDoId: Int!) : ToDoType
  }
`;
module.exports = toDoListType;