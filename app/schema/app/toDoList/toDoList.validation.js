
/**
 * The functions responsible for validating the toDoList type.
 * @module toDoList_validation
 */


module.exports = (db) => {
  const Op = db.Sequelize.Op;
  /**
   * Check validation of the toDoListId. It will return true if the toDoListId is real.
   * @param {number} id
   * @returns {boolean}
   * @example 
   *  await toDoListValidation.isIdValid(id)
   */
  const isIdValid = (id) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingToDoList = await db.toDoList.findOne({
        where: {
          id: id,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingToDoList !== null })
    });
  }

  /**
   * Check validation of many toDoListIds. It will return true if all toDoListIds are true.
   * @param {Array<number>} idArray
   * @returns {boolean}
   * @example 
   *  await toDoListValidation.areIdsValid([1, 2, 3])
   */
  const areIdsValid = (idArray) => {
    return new Promise(async (resolve, reject) => {

      const selectedToDoLists = await db.toDoList.findAndCountAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: idArray
          },
          isDeleted: false
        }
      })

      resolve({ result: idArray.length === selectedToDoLists.count })
    })
  }

  /**
   * If the name of a toDoList is already taken, then it returns true. If you want a name available for adding a toDoList, then you want false return.
   * @param {string} name
   * @returns {boolean}
   * @example 
   *  await toDoListValidation.isNameTaken("name")
   */
  const isNameTaken = (name) => {
    return new Promise(async (resolve, reject) => {

      const checkExistingToDoList = await db.toDoList.findOne({
        where: {
          name: name,
          isDeleted: false,
        },
      });

      resolve({ result: checkExistingToDoList !== null })
    })
  }


  return {
    isIdValid,
    areIdsValid,
    isNameTaken,
  }
}