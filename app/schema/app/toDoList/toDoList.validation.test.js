const makeUserValidation = require("./toDoList.validation");
const mockedDB = require("../../../models/mocked")

const toDoListValidation = makeUserValidation(mockedDB);

test("Check toDoList.validation isIdValid function.", async () => {
  const isIdValid = await toDoListValidation.isIdValid(1);

  expect(isIdValid.result).toBe(true)
})

test("Check toDoList.validation areIdsValid function.", async () => {
  const areIdsValid = await toDoListValidation.areIdsValid([1, 2, 3000000000000]);

  expect(areIdsValid.result).toBe(false)
})

test("Check toDoList.validation isNameTaken function.", async () => {
  const isNameTaken = await toDoListValidation.isNameTaken("user:manage");

  expect(isNameTaken.result).toBe(true)
})

