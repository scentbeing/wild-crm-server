const makeToDoListLogic = require("./toDoList.logic");
const mockedDB = require("../../../models/mocked")

const toDoListLogic = makeToDoListLogic(mockedDB);

test("Check toDoList.logic getOneById function.", async () => {
	const toDoList = await toDoListLogic.getOneById({ id: 1 });
  expect(toDoList.id).toBe(1)
  expect(toDoList.name).toMatch("to do list 1")
})

test("Check toDoList.logic getManyWithPagination function.", async () => {
	const toDoLists = await toDoListLogic.getManyWithPagination({ q: "read", page: 0, pageSize: 20 });
	expect(toDoLists.rows[0].name).toMatch("to do list 2")
})

test("Check toDoList.logic getManyWithPagination association with user.", async () => {
	const toDoLists = await toDoListLogic.getManyWithPagination({ page: 0, pageSize: 20, userId: 1 });
	expect(toDoLists.rows[0].name).toMatch("to do list 2")
})

test("Check toDoList.logic addOne", async () => {
	const toDoList = await toDoListLogic.addOne({ name: "test" });

	expect(toDoList.name).toMatch("test")
	expect(toDoList).toHaveProperty('id')
})

test("Check toDoList.logic addMany", async () => {
	const toDoLists = await toDoListLogic.addMany({ toDoListNamesArray: [{ name: "toDoList1" }, { name: "toDoList2" }] });

	expect(toDoLists[0].id).toBe(1)
})

test("Check toDoList.logic updateOne", async () => {
	const toDoList = await toDoListLogic.updateOne({ id: 1, name: "asdf" });

	expect(toDoList.name).toMatch("asdf")
})

test("Check toDoList.logic deleteOne", async () => {
	const toDoList = await toDoListLogic.deleteOne({ id: 1});

	expect(toDoList.isDeleted).toBe(true)
})
