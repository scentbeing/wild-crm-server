const config = require("../config/db.config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    // operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// app - toDo
db.toDo = require("./app/toDo/toDo.model")(sequelize, Sequelize);
db.toDoList = require("./app/toDo/toDoList.model")(sequelize, Sequelize);

// app - setting
db.settingEmail = require("./app/settings/settingEmail.model.js")(sequelize, Sequelize);
db.settingGeneral = require("./app/settings/settingGeneral.model.js")(sequelize, Sequelize);
db.settingPassword = require("./app/settings/settingPassword.model.js")(sequelize, Sequelize);
db.settingRequest = require("./app/settings/settingRequest.model.js")(sequelize, Sequelize);

// user
db.permission = require("./user/permission.model.js")(sequelize, Sequelize);
db.userRequest = require("./user/userRequest.model.js")(sequelize, Sequelize);
db.role = require("./user/role.model.js")(sequelize, Sequelize);
db.roleManyPermission = require("./user/roleManyPermission.model.js")(sequelize, Sequelize);
db.user = require("./user/user.model.js")(sequelize, Sequelize);
db.userManyPermission = require("./user/userManyPermission.model.js")(sequelize, Sequelize);
db.userProfile = require("./user/userProfile.model.js")(sequelize, Sequelize);
db.userManyRole = require("./user/userManyRole.model.js")(sequelize, Sequelize);

// utils
db.log = require("./system/log.model.js")(sequelize, Sequelize);



// User relationships

// User has a profile
db.user.hasOne(db.userProfile)
db.userProfile.belongsTo(db.user)

//User Many to Many with Permissions Part: 1
db.user.hasMany(db.userManyPermission)
db.userManyPermission.belongsTo(db.user)

//User Many to Many with Permissions Part: 2
db.permission.hasMany(db.userManyPermission)
db.userManyPermission.belongsTo(db.permission)

//User Many to Many with Role Part: 1
db.user.hasMany(db.userManyRole)
db.userManyRole.belongsTo(db.user)

//User Many to Many with Role Part: 2
db.role.hasMany(db.userManyRole)
db.userManyRole.belongsTo(db.role)

//Permissions Many to Many with Roles Part: 1
db.permission.hasMany(db.roleManyPermission)
db.roleManyPermission.belongsTo(db.permission)

//Permissions Many to Many with Roles Part: 2
db.role.hasMany(db.roleManyPermission)
db.roleManyPermission.belongsTo(db.role)


// To Do Relationships

//ToDoLists Many to Many with ToDo Part: 1
db.toDoList.hasMany(db.toDo)
db.toDo.belongsTo(db.toDoList)

// //ToDoLists Many to Many with ToDo Part: 2
// db.toDoList.hasMany(db.toDoListManyToDo)
// db.toDoListManyToDo.belongsTo(db.toDoList)


// user owns toDo List
db.user.hasMany(db.toDoList)
db.toDoList.belongsTo(db.user)


module.exports = db;
