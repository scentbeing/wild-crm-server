const { defaultsProperties } = require("../utils/model-properties.func");

module.exports = (sequelize, Sequelize) => {
  const UserManyRole = sequelize.define(
    "userManyRole",
    defaultsProperties(Sequelize, {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
    })
  );

  return UserManyRole;
};
