const defaultsProperties = (Sequelize, sequelizeDefinition) => {
  return Object.assign(sequelizeDefinition, {
    isDeleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  });
};

module.exports = {
  defaultsProperties,
};
