const { rule } = require("graphql-shield");
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const makeToDoLogic = require("../schema/app/toDo/toDo.logic")
const makeToDoListLogic = require("../schema/app/toDoList/toDoList.logic")
const makeUserLogic = require("../schema/user/user/user.logic");
const makeRoleLogic = require("../schema/user/role/role.logic");
const makePermissionLogic = require("../schema/user/permission/permission.logic.js");

const toDoLogic = makeToDoLogic(db)
const toDoListLogic = makeToDoListLogic(db)
const userLogic = makeUserLogic(db)
const roleLogic = makeRoleLogic(db)
const permissionLogic = makePermissionLogic(db)

const getClaim = (ctx) => {
  return new Promise(async (resolve, reject) => {
    try {

      if (ctx.user) {
        return resolve(ctx.user)
      }

      let token = ctx.headers["Authorization"] || ctx.headers["authorization"];

      if (!token) {
        return reject("No token provided!");
      }

      token = token.split(" ").filter((el) => el.length != 0)[1];

      const decoded = await jwt.verify(token, config.secret);

      const user = await userLogic.getOneById({
        id: decoded.id
      })

      if (!user) {
        return reject("Not Authorized!");
      }

      user.roleMany = await roleLogic.getManyWithPagination({
        userId: user.id
      })

      user.permisisonMany = await permissionLogic.getManyWithPagination({
        userId: user.id
      })

      ctx.user = user;

      resolve(user);
    } catch (err) {
      console.log(err);
      // reject(err.message);
      reject("Not Authorized!")
    }
  });
};

const isOpened = rule()(async (parent, args, ctx, info) => {
  return new Promise((resolve, reject) => {

    resolve(true)
  })
})

const isAuthenticated = rule()(async (parent, args, ctx, info) => {
  try {
    const user = await getClaim(ctx);

    return true;
  } catch (err) {
    console.log(err);
    return Error(err);
  }
});

const isAdmin = rule()(async (parent, args, ctx, info) => {
  try {
    const user = await getClaim(ctx);

    const isUserAnAdmin = user.roleMany.rows.filter(role => role.name === "admin").length > 0

    return isUserAnAdmin;
  } catch (err) {
    console.log(err);
    return Error(err);
  }
});

const isOwnerOfToDoList = rule()(async (parent, args, ctx, info) => {
  try {
    const user = await getClaim(ctx);

    let toDoList;

    if (args.id) {
      toDoList = await toDoListLogic.getOneById({ id: args.id })
    }

    if (args.toDoId) {
      const toDo = await toDoLogic.getOneById({ id: args.toDoId })

      toDoList = await toDoListLogic.getOneById({ id: toDo.toDoListId })
    }



    return user.id === toDoList.userId;
  } catch (err) {
    console.log(err);
    return Error(err);
  }
});


module.exports = {
  isAuthenticated,
  isAdmin,
  isOpened,
  isOwnerOfToDoList,
};
